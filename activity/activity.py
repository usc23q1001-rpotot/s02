full_name = "Rixielie Marie Potot"
age = 23
occupation = "Front End Designer"
movie = "Crash Landing on You"
rating = 100

print(f"I am {full_name} , and I am {age} years old, I work as a {occupation} , and my rating for {movie} is {rating}%")

num1 = 5
num2 = 17
num3 = 18

print(num1 * num2)
print(num1 < num3)
print(num3 + num2)